import pixi from 'pixi.js';

let app = new PIXI.Application({
    antialias: true,
    transparent: false,
    resolution: 1
  }
);

app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight);

document.querySelector(".gameContainer").appendChild(app.view);
